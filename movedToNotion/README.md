# README

Get ressources from my schools notes, like table for scales & exercices.

## Todo
- Splashscreen
- bottom menu bar with logo as hashiriya
- Gradient background


### GENERAL PURPOSE
#### STYLE
- How to generate watercolour design in flutter? inspiration : jeu de plateau Wingspan.

### CONTENT
#### ARTICLES
- Can we import Markdown data into flutter articles?

#### TOOLS
- Diagrams Builder.
- Pedal Presets.
- Metronome.
- Key generator.
- Circle of Fifth.

# Tasks

    Liste des tetra-cordes.
    Colorcharts (Ajouter teinte bleuté type atom?). 


Check to integrate MusicXml dans du markdown.

# Tools
- flat.io -> edit score on the go (web-browser).
- musescore (linux ok & phoneApp), finale, sibelius & guitar pro -> localy.


# WorFlow
- Scores Save as *.gpx/*.pdf & *.musiclXml.
check for files to save music, xml or gp or?


# Workshop
- Key Signature randomizer (& set beats as you want or random, dble-click or tap right for adding beat, dble-click or tap middle for subdivision & dble-click or tap left for substracting a beat. the last beat get all the subdivisions).
## Ear Training
- Play two notes (or more) randomly (low note on left and high on right) and guess the interval. Do a table of 3/4 or 6 possibles answers, then a mode where to guess thoses notes (if the first was right with same number of example and layout as x/x).
- version for Rythm.

