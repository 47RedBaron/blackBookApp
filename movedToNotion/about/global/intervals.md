# Intervals

## Create a class for all the intervals.

```
Class Intervals() {

b2 : 1
2 : 2
#2 : 3
b3 : 3
3 : 4
...
}
```
or 
```
{
    0 : "root",
    1 : "b2", 
    2 : "2",
    3 : {
     "b" : "b3",
     "♮" : null,
     "#" : "#2"   
    }
    ...
}


or 
{
    0 : {
     "b" : null,
     "♮" : "root",
     "#" : null   
    },
    1 : {
     "b" : null,
     "♮" : "b2",
     "#" : null   
    }, 
    2 : "2",
    3 : {
     "b" : "b3",
     "♮" : null,
     "#" : "#2"   
    }
    ...
}
```



## Create a method to get the interval from two notes 

```
public fonction getInterval(firstNote String, secondNote String) {
...
}
```
