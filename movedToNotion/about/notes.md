# NOTES

## Key Signature -App Section-
randomKey(){
//const key as a json file
// 'ID', 'flat', 'sharp'
degressifRandom(key.length)
}
randomScale(){
//import a Json file which contains M m melo, m harmo koto and modes
// 'ID', 'name', 'scale structure' -> {} or [] 
}
randomMode() {
// this way?
//degressif
}
randomTempo() {
}
randomProgression(key) {
}

## LAYOUT
- Main Page (Icons to select mode, 2 per Ligne).
-Top slide left right for sections or 3 Lines menu? if slide little dot under to know where we are.

## UX - USER EXPERIENCE
- Scroll bar fine en haut de l'écran de gauche à droite, de couleur.
- Sur les outils, le scroll down change la page (vue par vue), et non pas un scroll doux?

## MAIN SECTIONS
- Tuner (look for code on internet).
- Metronome (& chords layout added function from Tetra-Cordes).
- Key and Tonality Generator ((with metronome,  armature and cadence choice. Simple mode of full mode with scale written under it).
- Pif-O-Meter : Scale/Key/Tonality Generator (Tetra-Cordes).
- String Tension Calculator (gauge/scale/note; look for internet).
- Datadex (scales, CAGED...).

## MENU PARAMETERS
- [L || R] -> Orientation in menu for chords Diagrams and neck visualisation. 


## SCALES GENERATOR
    Elements.
- Tetra-Cordes.
- add key slider (# & b)?

    Layout (Top to Bottom).
- Lignes without key, symbol for the space between the note.
- Mix bouton to mix between two Tetra-Cordes and Slide Wheel for how many notes (5 to10?) -Slide Wheel : 3 number aligned vertically, center bigger than the others, two vertical bars on each side?-.
- the degrees type and the chords with simple 3 notes chords or full 4 notes chords (simple, 7th,sus,add...).
- The respective mode and being able to select it as a main scale. 

    Functions.
-Fonction midi when click on : play a note if one click. Keyboard nap 2 click until touch screen again.
- Ability to add this in a few bars. 

